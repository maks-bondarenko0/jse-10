package ru.t1.bondarenko.tm.service;

import ru.t1.bondarenko.tm.api.repository.ITaskRepository;
import ru.t1.bondarenko.tm.api.service.ITaskService;
import ru.t1.bondarenko.tm.enumerated.Status;
import ru.t1.bondarenko.tm.model.Project;
import ru.t1.bondarenko.tm.model.Task;

import java.util.List;

public final class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public Task add(Task task) {
        if (task == null) return null;
        return taskRepository.add(task);
    }

    @Override
    public void deleteAll() {
        taskRepository.deleteAll();
    }

    @Override
    public Task create(final String name) {
        if (name == null || name.isEmpty()) return null;
        final Task task = new Task();
        task.setName(name);
        return taskRepository.add(task);
    }

    @Override
    public Task create(final String name, final String description) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        return taskRepository.add(task);
    }

    @Override
    public Task findByID(final String id) {
        if (id == null || id.isEmpty()) return null;
        return taskRepository.findByID(id);
    }

    @Override
    public Task findByIndex(final Integer index) {
        if (index == null || index < 0) return null;
        return taskRepository.findByIndex(index);
    }

    @Override
    public List<Task> findAllByProjectID(final String projectId) {
        if (projectId == null || projectId.isEmpty()) return null;
        return taskRepository.findAllByProjectID(projectId);
    }

    @Override
    public Task delete(final Task task) {
        if (task == null) return null;
        return taskRepository.delete(task);
    }

    @Override
    public Task removeByID(final String id) {
        if (id == null || id.isEmpty()) return null;
        return taskRepository.removeByID(id);
    }

    @Override
    public Task removeByIndex(final Integer index) {
        if (index == null || index < 0) return null;
        return taskRepository.removeByIndex(index);
    }

    @Override
    public Task updateByID(final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) return null;
        if (name == null || name.isEmpty()) return null;
        Task task = taskRepository.findByID(id);
        if (task == null) return null;
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateByIndex(final Integer index, final String name, final String description) {
        if (index == null || index < 0) return null;
        if (name == null || name.isEmpty()) return null;
        Task task = taskRepository.findByIndex(index);
        if (task == null) return null;
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task changeTaskStatusById(final String id, final Status status) {
        if (id == null || id.isEmpty()) return null;
        final Task task = findByID(id);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeTaskStatusByIndex(Integer index, Status status) {
        if (index == null || index < 0) return null;
        final Task task = findByIndex(index);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

}
