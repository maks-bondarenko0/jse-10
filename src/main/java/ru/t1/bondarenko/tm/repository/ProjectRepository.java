package ru.t1.bondarenko.tm.repository;

import ru.t1.bondarenko.tm.api.repository.IProjectRepository;
import ru.t1.bondarenko.tm.model.Project;

import java.util.ArrayList;
import java.util.List;

public final class ProjectRepository implements IProjectRepository {

    private final List<Project> projects = new ArrayList<>();

    public ProjectRepository() {
    }

    @Override
    public List<Project> findAll() {
        return projects;
    }

    @Override
    public Project add(final Project project) {
        projects.add(project);
        return project;
    }

    @Override
    public void deleteAll() {
        projects.clear();
    }

    @Override
    public Project findByID(final String id) {
        for (final Project project : projects) {
            if (id.equals(project.getId())) return project;
        }
        return null;
    }

    @Override
    public Project findByIndex(final Integer index) {
        return projects.get(index);
    }

    @Override
    public Project delete(final Project project) {
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

    @Override
    public Project removeByID(final String id) {
        final Project project = findByID(id);
        if (project == null) return null;
        return delete(project);
    }

    @Override
    public Project removeByIndex(final Integer index) {
        final Project project = findByIndex(index);
        if (project == null) return null;
        return delete(project);
    }

    @Override
    public boolean existsById(final String id) {
        return findByID(id) != null;
    }

}
