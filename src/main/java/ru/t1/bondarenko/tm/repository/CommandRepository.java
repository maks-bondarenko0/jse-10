package ru.t1.bondarenko.tm.repository;

import ru.t1.bondarenko.tm.api.repository.ICommandRepository;
import ru.t1.bondarenko.tm.constant.ArgumentConstant;
import ru.t1.bondarenko.tm.constant.TerminalConstant;
import ru.t1.bondarenko.tm.model.Command;

public final class CommandRepository implements ICommandRepository {

    private static final Command INFO = new Command(
            TerminalConstant.INFO, ArgumentConstant.INFO,
            "Show developer info."
    );

    private static final Command VERSION = new Command(
            TerminalConstant.VERSION, ArgumentConstant.VERSION,
            "Show application version."
    );

    private static final Command HELP = new Command(
            TerminalConstant.HELP, ArgumentConstant.HELP,
            "Show application commands."
    );

    private static final Command COMMANDS = new Command(
            TerminalConstant.COMMANDS, ArgumentConstant.COMMANDS,
            "Show application commands."
    );

    private static final Command ARGUMENTS = new Command(
            TerminalConstant.ARGUMENTS, ArgumentConstant.ARGUMENTS,
            "Show application arguments."
    );

    private static final Command EXIT = new Command(
            TerminalConstant.EXIT, null,
            "Close application."
    );

    private static final Command PROJECT_CREATE = new Command(
            TerminalConstant.PROJECT_CREATE, null,
            "Create new project."
    );

    private static final Command PROJECT_LIST = new Command(
            TerminalConstant.PROJECT_LIST, null,
            "Show project list."
    );

    private static final Command PROJECT_CLEAR = new Command(
            TerminalConstant.PROJECT_CLEAR, null,
            "Remove all projects."
    );

    private static final Command PROJECT_SHOW_BY_ID = new Command(
            TerminalConstant.PROJECT_SHOW_BY_ID, null, "Show Project by ID."
    );

    private static final Command PROJECT_SHOW_BY_INDEX = new Command(
            TerminalConstant.PROJECT_SHOW_BY_INDEX, null, "Show Project by index."
    );

    private static final Command PROJECT_UPDATE_BY_ID = new Command(
            TerminalConstant.PROJECT_UPDATE_BY_ID, null, "Update Project by ID."
    );

    private static final Command PROJECT_UPDATE_BY_INDEX = new Command(
            TerminalConstant.PROJECT_UPDATE_BY_INDEX, null, "Update Project by index."
    );

    private static final Command PROJECT_REMOVE_BY_ID = new Command(
            TerminalConstant.PROJECT_REMOVE_BY_ID, null, "Delete Project by ID."
    );

    private static final Command PROJECT_REMOVE_BY_INDEX = new Command(
            TerminalConstant.PROJECT_REMOVE_BY_INDEX, null, "Delete Project by index."
    );

    private static final Command PROJECT_START_BY_ID = new Command(
            TerminalConstant.PROJECT_START_BY_ID, null, "Start Project by ID."
    );

    private static final Command PROJECT_START_BY_INDEX = new Command(
            TerminalConstant.PROJECT_START_BY_INDEX, null, "Start Project by index."
    );

    private static final Command PROJECT_COMPLETE_BY_ID = new Command(
            TerminalConstant.PROJECT_COMPLETE_BY_ID, null, "Complete Project by ID."
    );

    private static final Command PROJECT_COMPLETE_BY_INDEX = new Command(
            TerminalConstant.PROJECT_COMPLETE_BY_INDEX, null, "Complete Project by index."
    );

    private static final Command PROJECT_CHANGE_STATUS_BY_ID = new Command(
            TerminalConstant.PROJECT_CHANGE_STATUS_BY_ID, null, "Change Project Status by ID."
    );

    private static final Command PROJECT_CHANGE_STATUS_BY_INDEX = new Command(
            TerminalConstant.PROJECT_CHANGE_STATUS_BY_INDEX, null, "Change Project Status by index."
    );

    private static final Command TASK_CREATE = new Command(
            TerminalConstant.TASK_CREATE, null,
            "Create new task."
    );

    private static final Command TASK_LIST = new Command(
            TerminalConstant.TASK_LIST, null,
            "Show task list."
    );

    private static final Command TASK_CLEAR = new Command(
            TerminalConstant.TASK_CLEAR, null,
            "Remove all tasks."
    );

    private static final Command TASK_SHOW_BY_ID = new Command(
            TerminalConstant.TASK_SHOW_BY_ID, null, "Show Task by ID."
    );

    private static final Command TASK_SHOW_BY_INDEX = new Command(
            TerminalConstant.TASK_SHOW_BY_INDEX, null, "Show Task by index."
    );

    private static final Command TASK_SHOW_BY_PROJECT_ID = new Command(
            TerminalConstant.TASK_SHOW_BY_PROJECT_ID, null, "Show Task by Project ID."
    );

    private static final Command TASK_UPDATE_BY_ID = new Command(
            TerminalConstant.TASK_UPDATE_BY_ID, null, "Update Task by ID."
    );

    private static final Command TASK_UPDATE_BY_INDEX = new Command(
            TerminalConstant.TASK_UPDATE_BY_INDEX, null, "Update Task by index."
    );

    private static final Command TASK_REMOVE_BY_ID = new Command(
            TerminalConstant.TASK_REMOVE_BY_ID, null, "Delete Task by ID."
    );

    private static final Command TASK_REMOVE_BY_INDEX = new Command(
            TerminalConstant.TASK_REMOVE_BY_INDEX, null, "Delete Task by index."
    );

    private static final Command TASK_BIND_TO_PROJECT = new Command(
            TerminalConstant.TASK_BIND_TO_PROJECT, null, "Bind Task to Project."
    );

    private static final Command TASK_UNBIND_FROM_PROJECT = new Command(
            TerminalConstant.TASK_UNBIND_FROM_PROJECT, null, "Unbind Task from Project."
    );

    private static final Command TASK_START_BY_ID = new Command(
            TerminalConstant.TASK_START_BY_ID, null, "Start Task by ID."
    );

    private static final Command TASK_START_BY_INDEX = new Command(
            TerminalConstant.TASK_START_BY_INDEX, null, "Start Task by index."
    );

    private static final Command TASK_COMPLETE_BY_ID = new Command(
            TerminalConstant.TASK_COMPLETE_BY_ID, null, "Complete Task by ID."
    );

    private static final Command TASK_COMPLETE_BY_INDEX = new Command(
            TerminalConstant.TASK_COMPLETE_BY_INDEX, null, "Complete Task by index."
    );

    private static final Command TASK_CHANGE_STATUS_BY_ID = new Command(
            TerminalConstant.TASK_CHANGE_STATUS_BY_ID, null, "Change Task Status by ID."
    );

    private static final Command TASK_CHANGE_STATUS_BY_INDEX = new Command(
            TerminalConstant.TASK_CHANGE_STATUS_BY_INDEX, null, "Change Task Status by index."
    );

    private static final Command[] TERMINAL_COMMANDS = new Command[]{
            INFO, VERSION, HELP, COMMANDS, ARGUMENTS,

            PROJECT_CREATE, PROJECT_LIST, PROJECT_CLEAR,
            PROJECT_SHOW_BY_ID, PROJECT_SHOW_BY_INDEX,
            PROJECT_UPDATE_BY_ID, PROJECT_UPDATE_BY_INDEX,
            PROJECT_REMOVE_BY_ID, PROJECT_REMOVE_BY_INDEX,
            PROJECT_START_BY_ID, PROJECT_START_BY_INDEX,
            PROJECT_COMPLETE_BY_ID, PROJECT_COMPLETE_BY_INDEX,
            PROJECT_CHANGE_STATUS_BY_ID, PROJECT_CHANGE_STATUS_BY_INDEX,

            TASK_CREATE, TASK_LIST, TASK_CLEAR,
            TASK_SHOW_BY_ID, TASK_SHOW_BY_INDEX, TASK_SHOW_BY_PROJECT_ID,
            TASK_UPDATE_BY_ID, TASK_UPDATE_BY_INDEX,
            TASK_REMOVE_BY_ID, TASK_REMOVE_BY_INDEX,
            TASK_BIND_TO_PROJECT, TASK_UNBIND_FROM_PROJECT,
            TASK_START_BY_ID, TASK_START_BY_INDEX,
            TASK_COMPLETE_BY_ID, TASK_COMPLETE_BY_INDEX,
            TASK_CHANGE_STATUS_BY_ID, TASK_CHANGE_STATUS_BY_INDEX,

            EXIT
    };

    @Override
    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    };

}
